//
//  DLDemoData.m
//  Delivery
//
//  Created by iMac on 26/4/17.
//  Copyright © 2017 Braziosdev. All rights reserved.
//

#import "DLDemoData.h"

@implementation DLDemoData

+(CKPersonModel *)getDemoPerson{

    CKPersonModel *person = [[CKPersonModel alloc] init];
    [person setSession:TRUE];
    [person setPersonId:0];
    [person setName:@"Demo"];
    [person setTimezone:[[NSTimeZone localTimeZone] name]];
    [person setMobile:@"040000000000"];
    [person setEmail:@"demo@asyncme.com"];
    [person setToken:@"demotoken"];
    [person setDirectionsOption:0];
    [person setLocation:[[CLLocation alloc] initWithLatitude:fakeLat longitude:fakeLat]];
    
    return person;
}

+(CKCompanyModel *)getDemoCompany{
    
    CKCompanyModel *company = [[CKCompanyModel alloc] init];
    [company setCompanyId:0];
    [company setAdminId:0];
    [company setCompanyName:@"Demo"];
    
    return company;
}

+(DLRouteModel *)getDemoRouteData{

    DLRouteModel *route = [[DLRouteModel alloc] init];
    [route setRouteId:0];
    [route setName:@"Demo route"];
    [route setTime:[CKCoreTime GetCurrentTime] + 3600];
    [route setTimezone:CKPerson.timezone];
    [route setCompanyId:CKCompany.companyId];
    [route setPersonId:CKPerson.personId];
    [route setQtdCustomers:3];

     return route;
    
}

+(DLCustomerModel *)getDemoCustomer1{
    
    DLCustomerModel *customer = [[DLCustomerModel alloc] init];
    [customer setName:@"Josh Smith"];
    [customer setAddress:@"14 Swanston street, Melbourne, 3000 Vic"];
    [customer setOrder:0];
    [customer setMobile:@"04000000000"];
    [customer setNote:@"1 item"];
    [customer setCustomerId:0];
    [customer setRouteMapId:0];
    [customer setLat:-37.022222];
    [customer setLng:145.000000];
    [customer setLocation:[[CLLocation alloc] initWithLatitude:customer.lat longitude:customer.lng]];
    
    return customer;
}

+(DLCustomerModel *)getDemoCustomer2{
    
    DLCustomerModel *customer = [[DLCustomerModel alloc] init];
    [customer setName:@"Mary Stuart"];
    [customer setAddress:@"1432 Collins street, Melbourne, 3000 Vic"];
    [customer setOrder:0];
    [customer setMobile:@"04000000001"];
    [customer setNote:@"Call before"];
    [customer setCustomerId:1];
    [customer setRouteMapId:1];
    [customer setLat:-37.422222];
    [customer setLng:145.100000];
    [customer setLocation:[[CLLocation alloc] initWithLatitude:customer.lat longitude:customer.lng]];
    
    return customer;
    
}

+(DLCustomerModel *)getDemoCustomer3{
    
    DLCustomerModel *customer = [[DLCustomerModel alloc] init];
    [customer setName:@"Edward Lyn"];
    [customer setAddress:@"44 lygon street, Melbourne, 3000 Vic"];
    [customer setOrder:0];
    [customer setMobile:@"04000000002"];
    [customer setNote:@"Leave on front"];
    [customer setCustomerId:2];
    [customer setRouteMapId:2];
    [customer setLat:-37.622222];
    [customer setLng:145.200000];
    [customer setLocation:[[CLLocation alloc] initWithLatitude:customer.lat longitude:customer.lng]];
    
    return customer;
    
}

@end
