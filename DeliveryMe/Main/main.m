//
//  main.m
//  Clockme
//
//  Created by iMac on 29/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CKAppDelegate class]));
    }
}
