//
//  AppDelegate.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKLoginModel.h"

@interface CKAppDelegate : UIResponder <CKCoreLocationDelegate,CKCoreDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
