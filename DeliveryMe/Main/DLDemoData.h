//
//  DLDemoData.h
//  Delivery
//
//  Created by iMac on 26/4/17.
//  Copyright © 2017 Braziosdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLDemoData : NSObject
+(CKPersonModel *)getDemoPerson;
+(CKCompanyModel *)getDemoCompany;
+(DLRouteModel *)getDemoRouteData;
+(DLCustomerModel *)getDemoCustomer1;
+(DLCustomerModel *)getDemoCustomer2;
+(DLCustomerModel *)getDemoCustomer3;

@end
