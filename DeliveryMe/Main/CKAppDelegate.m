//
//  AppDelegate.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKAppDelegate.h"

@implementation CKAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    //Handle local notification implementation
    UILocalNotification *localNotification = [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotification) {
        [self application:application didReceiveLocalNotification:localNotification];
    }
    
    //Allow the app to check battery level
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    
    //IMPORTANT - LOAD USER INFO FROM MEMORY BEFORE PROCCEDING
    [[CKLoginModel sharedInstance] BeginCKSession];
    
    //Start fabric
    [Fabric with:@[CrashlyticsKit]];
    [self customizeAppAppearance];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0){
        [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:600];
    }
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun] || !CKPerson.session) {
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kFirstRun];
        [self.window setRootViewController:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"loginView"]];
    }else{
        [self.window setRootViewController:[[CKMain getStoryBoard] instantiateInitialViewController]];
    }
    
    return YES;
}

- (void)applicationDidFinishLaunching:(UIApplication *)app {

    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    [app registerForRemoteNotifications];
    
}

- (void)customizeAppAppearance{
    
    //Custom colors
    //[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    //[[UINavigationBar appearance] setTitleTextAttributes:([NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:20],NSFontAttributeName,nil])];

    //Segnment font attribute
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:12],NSFontAttributeName,[UIColor grayColor],NSForegroundColorAttributeName,nil];
    [[UISegmentedControl appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    [[UISegmentedControl appearance] setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    
    // Segmented Controls within Toolbars
    // Unselected background
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"segmentInactive.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    // Selected background
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"segmentActive.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    // Image between two unselected segments
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmentBothInactive.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    // Image between segment selected on the left and unselected on the right
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmentLeftActiveRightInactive.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    // Image between segment selected on the right and unselected on the left
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmentRightActiveLeftInactive.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    //TableviewCell checkmark color
    [[UITableViewCell appearance] setTintColor:[CKColor CKDefaultColor]];
    
    UIImage *minImage = [[UIImage imageNamed:@"slider-track-fill.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
    UIImage *maxImage = [UIImage imageNamed:@"slider-track.png"];
    UIImage *thumbImage = [UIImage imageNamed:@"slider-cap.png"];
    [[UISlider appearance] setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbImage forState:UIControlStateHighlighted];
        
}

/**
 Method delegate to update the user device id on database
 @param app         default
 @param deviceToken default
 */
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:kUserDeviceToken];
    [[CKLoginModel sharedInstance] RegisterDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    // Get the notifications types that have been allowed, do whatever with them
    //UIUserNotificationType allowedTypes = [notificationSettings types];
    //NSLog(@"Registered for notification types: %lu", (unsigned long)allowedTypes);
    
    // You can get this setting anywhere in your app by using this:
    //UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    //NSLog(@"Allow push Settings: %@",currentSettings);
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err{
    [[[UIAlertView alloc] initWithTitle:@"" message:[err localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application{
    //Stop tracking method when user enter the app again to avoid multiple insertion of data
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    
    //IMPORTANT - LOAD USER INFO FROM MEMORY BEFORE PROCCEDING
    //[[CKLoginModel sharedInstance] BeginCKSession];
    [[CKCoreLocation sharedInstance] initShareLocationManager];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application{

}

#pragma NOTIFICATION HANDLER
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    
    [[[UIAlertView alloc] initWithTitle:@"" message:notification.alertBody delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    [[[UIAlertView alloc] initWithTitle:@"" message:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
}

@end
