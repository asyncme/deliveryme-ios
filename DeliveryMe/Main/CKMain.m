//
//  Constants.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKMain.h"

CKPersonModel *CKPerson;
DLRouteModel *DLRoute;
CKCompanyModel *CKCompany;

@implementation CKMain

+(UIStoryboard *)getStoryBoard{

    UIStoryboard *st = [UIStoryboard storyboardWithName:kStoryBoardMain bundle:nil];
    return st;
}

+(void)TakeUserToLogin{
    
    UIViewController *loginView = [[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"LoginView"];
    [loginView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [rootViewController presentViewController:loginView animated:YES completion:nil];
    
}

@end
