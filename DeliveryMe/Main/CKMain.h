//
//  Constants.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

//Main classes
#import "DLMenuViewController.h"
#import "CKViewController.h"

//Core classes
#import "CKCoreWebService.h"
#import "CKCoreLocation.h"
#import "CKCoreTime.h"
#import "CKColor.h"
#import "CKAnimation.h"
#import "CKKeys.h"
#import "CKWebService.h"
#import "CKLang.h"
#import "CKUtility.h"

//SB models
#import "CKLoginModel.h"
#import "CKPersonModel.h"
#import "DLCustomerModel.h"
#import "DLRouteModel.h"
#import "CKCompanyModel.h"

//Global variables
extern CKPersonModel *CKPerson;
extern DLRouteModel *DLRoute;
extern CKCompanyModel *CKCompany;

@interface CKMain : NSObject

+(UIStoryboard *)getStoryBoard;
+(void)TakeUserToLogin;

@end
