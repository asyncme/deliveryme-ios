//
//  ViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "CKTableViewCell.h"

@interface CKProfileViewController : UIViewController <UIAlertViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CKCoreDelegate>

@property (strong, nonatomic) NSArray *arrayProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITableView *tblContent;

@property (strong, nonatomic) IBOutlet UISegmentedControl *sgmNavigation;

-(IBAction)Background:(id)sender;
-(IBAction)ShowMenu:(id)sender;
-(IBAction)ChangeNavigation:(id)sender;

@end
