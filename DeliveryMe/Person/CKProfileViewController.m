//
//  ViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKProfileViewController.h"

@interface CKProfileViewController (){
    CKCoreWebService *CK;
    NSMutableArray *arrayAction;
    UIImageView *imgBlurBg;
    BOOL isLoading,ended,canHandle;
    int lastRecord;
}

@end

@implementation CKProfileViewController

@synthesize txtEmail,txtName,txtMobile;
@synthesize arrayProfile,tblContent,sgmNavigation;

- (void)viewDidLoad{
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
    
    
    //Set title for view
    [self setTitle:@"My Profile"];
    
    //[btnSave setHidden:TRUE];
    [txtEmail setDelegate:self];
    [txtName setDelegate:self];
    [txtMobile setDelegate:self];
    
    [self DisableTextField];
    
    arrayAction = [[NSMutableArray alloc] init];
    isLoading = false;
    ended = false;

    [CK setDelegate:self];
    txtEmail.text = CKPerson.email;
    txtName.text = CKPerson.name;
    txtMobile.text = CKPerson.mobile;
    txtEmail.enabled = false;
    
    //Check if google is available to populate segment
    [sgmNavigation removeAllSegments];
    
    canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps:"]];
    if (canHandle) {
        [sgmNavigation insertSegmentWithTitle:@"Google Maps" atIndex:0 animated:TRUE];
        [sgmNavigation setTag:1];
    }
    
    [sgmNavigation insertSegmentWithTitle:@"Apple Maps" atIndex:1 animated:TRUE];
    [sgmNavigation setTag:2];
    
    // Set choosen direction selected
    if (CKPerson.directionsOption != 0) {
        if (canHandle) {
            if (CKPerson.directionsOption == 1) {
                [sgmNavigation setSelectedSegmentIndex:0];
            }else{
                [sgmNavigation setSelectedSegmentIndex:1];
            }
        }else{
            [sgmNavigation setSelectedSegmentIndex:0];
        }
    }
    
}

-(IBAction)ChangeNavigation:(UISegmentedControl *)sender{
    
    if (canHandle) {
        if (sender.selectedSegmentIndex == 0) {
            CKPerson.directionsOption = 1;
        }else{
            CKPerson.directionsOption = 2;
        }
    }else{
        CKPerson.directionsOption = 2;
    }
    
    [CKPersonModel UpdateUserInfo];
    
}

/**
 Method to fetch user info, if a profileId is assign it will look upon the user detail, otherwise it will load the app user profile.
 @param animated default
 */
-(void)viewDidAppear:(BOOL)animated{
//    [self fetchData];
}

-(IBAction)ShowMenu:(id)sender{
    
    DLMenuViewController *CKMenu = (DLMenuViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"CKMenu"];
    [CKMenu setImgReceive:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleLight]];
    [CKMenu setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:CKMenu animated:YES completion:nil];
    
}

-(void)fetchData{
    if (!isLoading) {
        [[CKViewController sharedInstance] showClockInView:self.view];
    }
    
    if (!ended) {
        [self performSelectorInBackground:@selector(RequestPersonActions) withObject:nil];
    }
}

/**
 Method to Fetch data with user log
 */
-(void)RequestPersonActions{

    NSString *myRequestString;
    int limit = 20;
    
    if ([arrayAction count] == 0) {
        myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&limit=%i",CKPerson.personId,CKPerson.token,limit];
        [CK CoreWebService:kRoutePersonActions withRequest:myRequestString];
    }
}

#pragma mark - SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LogUserSuccess) {
            
            for (NSDictionary *dic in [dictionary valueForKey:@"actions"]) {

                
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                isLoading = false;
                [tblContent reloadData];
            });
            
        }
        
    }else{
     
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] != SBWebServiceResponseCode_LogNoLog) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }

    }
    
    [[CKViewController sharedInstance] removeClock];
}

//-(NSArray *)SortActionById:(NSArray *)toSort{
//    
//    @try {
//        
//        NSSortDescriptor *sortDistance = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES comparator:^NSComparisonResult(id obj1, id obj2){
//            CKActionModel *CKAction1 = (CKActionModel *)obj1;
//            CKActionModel *CKAction2 = (CKActionModel *)obj2;
//            
//            return [[NSNumber numberWithDouble:CKAction1.actionNoteId] compare:[NSNumber numberWithDouble:CKAction2.actionNoteId]];
//            
//        }];
//        
//        return [toSort sortedArrayUsingDescriptors:@[sortDistance]];
//        
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Error: %@",exception);
//        return nil;
//    }
//}

/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKViewController sharedInstance] removeClock];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayAction count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *myIdentifier = @"cell";
    
    CKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    if (cell == nil) {
        cell = [[CKTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
    //Validate if cell has been proper initialized before continue
    if (!cell) {
        return nil;
    }
    
    @try {
        
        
        //Manage cell elements
        [cell.lblFooterSubtitle setHidden:TRUE];
        [cell.lblFooterTitle setHidden:TRUE];
        [cell.txtActionNote setHidden:FALSE];
        
//        [cell.btnAction setTag:indexPath.row];
//        [cell.btnAction addTarget:self action:@selector(AddActionNote:) forControlEvents:UIControlEventTouchUpInside];
     
        //Load more data after reaching the bottom
        if ([arrayAction count] - 1 == indexPath.row) {
            if (!isLoading) {
                isLoading = true;
                [self fetchData];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    return cell;
}



#pragma -mark ShowPassword change
-(void)ShowAddActionNoteView{
    [self AddBlurBg];
}

-(void)HideActionNoteView{
    [imgBlurBg setHidden:TRUE];
    imgBlurBg = nil;
    [self Background:self];
}

-(void)CancelctionNote{
    [self HideActionNoteView];
}

-(void)AddBlurBg{
    if (imgBlurBg == nil) {
        
        @try {
            imgBlurBg = [[UIImageView alloc] initWithImage:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleWhiter]];
            if (imgBlurBg) {                
                [UIView animateWithDuration:0.2 animations:^() {
                    [imgBlurBg setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    [imgBlurBg setAlpha:0];
//                    [self.view insertSubview:imgBlurBg belowSubview:actionNoteView.view];
                    [imgBlurBg setAlpha:1];
                }];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Error: %@",exception);
        }
    }
}

/**
 Method delegate for UIAlertView
 
 @param alertView   alert
 @param buttonIndex button pressed
 */
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}

/**
 Methdo to enable all editable textField
 */
-(void)EnableTextField{
    [txtName setEnabled:TRUE];
    [txtMobile setEnabled:TRUE];
    [txtEmail setEnabled:TRUE];
    
    //Set white color background
    [txtName setBackgroundColor:[UIColor whiteColor]];
    [txtMobile setBackgroundColor:[UIColor whiteColor]];
    [txtEmail setBackgroundColor:[UIColor whiteColor]];
}

/**
 Methdo to disable all editable textField
 */
-(void)DisableTextField{
    [txtName setEnabled:FALSE];
    [txtMobile setEnabled:FALSE];
    [txtEmail setEnabled:FALSE];
    
    //Set white color background
    [txtName setBackgroundColor:[CKColor lightGrayColor]];
    [txtMobile setBackgroundColor:[CKColor lightGrayColor]];
    [txtEmail setBackgroundColor:[CKColor lightGrayColor]];}

/**
 Method when user tap on background
 
 @param sender default
 */
-(IBAction)Background:(id)sender{
    [txtEmail resignFirstResponder];
    [txtName resignFirstResponder];
    [txtMobile resignFirstResponder];
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    });
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[CKViewController sharedInstance] removeClock];
}
@end
