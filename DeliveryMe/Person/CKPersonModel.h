//
//  SBUserModel.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 23/02/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CKPersonModel : NSObject

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *token;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *mobile;
@property (copy, nonatomic) NSString *deviceToken;
@property (copy, nonatomic) NSString *timezone;
@property (copy, nonatomic) CLLocation *location;

@property int personId,accessLevel,directionsOption;
@property BOOL session,leftGeofence;


// directionsOption
// 0 - no defined, 1 - apple maps, 2 - google maps

-(void)initSBSession;
-(id)initObjectWithDictionary:(NSDictionary *)personInfo;
-(void)initCKPersonFromMemory;
+(void)UpdateUserInfo;

typedef enum{
    CKPersonManager=1,
    CKPersonNormal
}CKAccessLevelType;

@end
