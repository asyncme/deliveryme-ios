//
//  SBSiteModel.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 26/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "DLRouteModel.h"

@implementation DLRouteModel

-(id)initObjectWithDictionary:(NSDictionary *)routeInfo{
    
    @try {
        
        [self setName:[CKUtility getStringvalueForKey:[routeInfo valueForKey:@"name"]]];
        [self setTimezone:[CKUtility getStringvalueForKey:[routeInfo valueForKey:@"timezone"]]];
        [self setRouteId:[CKUtility getIntValueForKey:[routeInfo valueForKey:@"routeId"]]];
        [self setCompanyId:[CKUtility getIntValueForKey:[routeInfo valueForKey:@"companyId"]]];
        [self setPersonId:[CKUtility getIntValueForKey:[routeInfo valueForKey:@"personId"]]];
        [self setQtdCustomers:[CKUtility getIntValueForKey:[routeInfo valueForKey:@"qtdCustomers"]]];
        [self setTime:[CKUtility getDoubleValueForKey:[routeInfo valueForKey:@"time"]]];
        [self setLocation:[[CLLocation alloc] initWithLatitude:[CKUtility getDoubleValueForKey:[routeInfo valueForKey:@"lat"]] longitude:[CKUtility getDoubleValueForKey:[routeInfo valueForKey:@"lng"]]]];
        
        NSMutableArray *customers = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [routeInfo valueForKey:@"customers"]) {
            [customers addObject:[[DLCustomerModel alloc] initObjectWithDictionary:dic]];
        }

        [self setArrayCustomers:customers];
        
        return self;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

-(void)initDLRouteFromMemory{
    
    @try {
        
        NSData *archaviedData = [[NSUserDefaults standardUserDefaults] valueForKey:kCKRouteModel];
        if (archaviedData) {
            NSMutableDictionary *arrayRoute = [NSKeyedUnarchiver unarchiveObjectWithData:archaviedData];
            if ([arrayRoute count] != 0) {
                DLRoute = [self initObjectWithDictionary:arrayRoute];
            }
        }else{
            //Support for users that are migrating from the previous version
            DLRoute = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKRouteModel]];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        
        //Support for users that are migrating from the previous version
        DLRoute = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKRouteModel]];
    }
    
}

+(void)UpdateRouteInfo{
    
    @try {
        NSMutableDictionary *updateRoute = [[NSMutableDictionary alloc] init];
        [updateRoute setValue:DLRoute.name forKey:@"name"];
        [updateRoute setValue:DLRoute.timezone forKey:@"timezone"];
        [updateRoute setValue:DLRoute.customers forKey:@"customers"];
        [updateRoute setValue:[NSNumber numberWithInt:DLRoute.routeId] forKey:@"routeId"];
        [updateRoute setValue:[NSNumber numberWithInt:DLRoute.personId] forKey:@"personId"];
        [updateRoute setValue:[NSNumber numberWithInt:DLRoute.companyId] forKey:@"companyId"];
        [updateRoute setValue:[NSNumber numberWithInt:DLRoute.qtdCustomers] forKey:@"qtdCustomers"];
        [updateRoute setValue:[NSNumber numberWithDouble:DLRoute.time] forKey:@"time"];
        [updateRoute setValue:[NSNumber numberWithDouble:DLRoute.location.coordinate.latitude] forKey:@"lat"];
        [updateRoute setValue:[NSNumber numberWithDouble:DLRoute.location.coordinate.longitude] forKey:@"lng"];
        [updateRoute setValue:DLRoute.customers forKey:@"customers"];
        
        NSLog(@"----> Updated route model <----");
        
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:updateRoute];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setObject:dataObj forKey:kCKRouteModel];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}


@end
