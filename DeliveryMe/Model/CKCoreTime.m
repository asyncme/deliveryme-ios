//
//  SBCoreTime.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 23/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKCoreTime.h"

@implementation CKCoreTime


static CKCoreTime *sharedInstance = nil;

+(CKCoreTime *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

/**
 Method that will return current time
 
 @return current time
 */
+(double)GetCurrentTime{
    return [[NSDate date] timeIntervalSince1970];
}

+(NSDate *)GetDateFromString:(NSString *)date{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:SS"];
    NSDate *dte = [dateFormat dateFromString:date];
    return dte;
}

/**
 Method to convert the given time to device local time
 
 @param time given time
 
 @return string with human interface
 */
+(NSString *)ConvertToLocalTimeDate:(double)time{
    
    if (!time) {
        return @"";
    }
    
    NSDate *toDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *formatter= [[NSDateFormatter alloc]init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString *date = [formatter stringFromDate:toDate];
    
    return date;
}

/**
 Method to convert the given time to device local time
 
 @param time given time
 
 @return string with human interface
 */
+(NSString *)ConvertToLocalTime:(double)time{
    
    if (!time) {
        return @"";
    }
    
    NSDate *toDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *formatter= [[NSDateFormatter alloc]init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"dd/MM/yy HH:mm"];
    NSString *date = [formatter stringFromDate:toDate];
    
    return date;
}

/**
 Method to convert the given time to device local time with seconds
 
 @param time given time
 
 @return string with human interface
 */
+(NSString *)ConvertToLocalTimeWithSeconds:(double)time{
    
    @try {
        if (!time) {
            return @"";
        }
        
        NSDate *toDate = [NSDate dateWithTimeIntervalSince1970:time];
        NSDateFormatter *formatter= [[NSDateFormatter alloc]init];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateFormat:@"dd/MM/yy HH:mm:ss"];
        NSString *date = [formatter stringFromDate:toDate];
        
        return date;
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        return 0;
    }
    
}


/**
 Method to get the next time the user has to report
 
 @param stringDate date in string
 
 @return return the next hour + 2
 */
+(NSString *)GetTimeFromDate:(NSString *)stringDate{
    NSArray *extract = [stringDate componentsSeparatedByString:@" "];
    NSArray *split = [[extract objectAtIndex:1] componentsSeparatedByString:@":"];
    double hour;
    hour = [[split objectAtIndex:0] intValue];
    hour = hour;
    
    if (hour > 23) {
        hour = hour - 24;
        return [NSString stringWithFormat:@"%.f:%@",hour,[split objectAtIndex:1]];
    }else{
        return [NSString stringWithFormat:@"%.f:%@",hour,[split objectAtIndex:1]];
    }
    
}

/**
 Method to convert in hours the amount of time stamp
 
 @return quantity of hours
 */
+(NSString *)GetHoursByTimeStamp:(double)time{
    
    int hours = time / SECONDS_IN_AHOUR;
    
    if (hours > 0) {
        
        int mod = ((int)time) % SECONDS_IN_AHOUR;
        int min = mod / MINUTES_IN_AHOUR;
        NSString *minutes;
        
        if (min < 10) {
            minutes= [NSString stringWithFormat:@"0%i",min];
        }else{
            minutes = [NSString stringWithFormat:@"%i",min];
        }
        
        if (hours > 1) {
            return [NSString stringWithFormat:@"%i:%@ hours",hours,minutes];
        }else{
            return [NSString stringWithFormat:@"%i:%@ hour",hours,minutes];
        }
        
    }else{
        
        int minutes = time / MINUTES_IN_AHOUR;
        
        if (minutes > 0) {
            return [NSString stringWithFormat:@"%i minutes",minutes];
        }else{
            return [NSString stringWithFormat:@"%i minute",minutes];
        }
        
    }
    
}

/**
 Method to convert in hours the amount of time stamp
 
 @return quantity of hours
 */
+(NSString *)GetTimeAgoFromTimeStamp:(double)time{
    
    int timeDif = [self GetCurrentTime] - time;
    
    int hours = timeDif / SECONDS_IN_AHOUR;
    int mod = ((int)timeDif) % SECONDS_IN_AHOUR;
    int minutes = mod / MINUTES_IN_AHOUR;
    
    if (hours > 0) {
        
        NSString *min;
        
        if (minutes < 10) {
            min = [NSString stringWithFormat:@"0%i",minutes];
        }else{
            min = [NSString stringWithFormat:@"%i",minutes];
        }
        
        if (hours > 1) {
            return [NSString stringWithFormat:@"%i:%@ hours",hours,min];
        }else{
            return [NSString stringWithFormat:@"%i:%@ hour",hours,min];
        }
        
    }else if(minutes > 0){
        
        if (minutes > 0) {
            return [NSString stringWithFormat:@"%i minutes",minutes];
        }else{
            return [NSString stringWithFormat:@"%i minute",minutes];
        }
        
    }else{
        
        if (timeDif > 0) {
            return [NSString stringWithFormat:@"%i seconds",timeDif];
        }else{
            return @"Just now";
        }
        
    }
    
}

/**
 Method to convert in hours the amount of time stamp
 
 @return quantity of hours
 */
+(NSString *)GetCountDownTimeByTimeStamp:(double)time{
    
    int hours = time / SECONDS_IN_AHOUR;
    int mod = ((int)time) % SECONDS_IN_AHOUR;
    int min = mod / MINUTES_IN_AHOUR;
    int sec = mod % MINUTES_IN_AHOUR;
    NSString *minutes;
    NSString *seconds;
    
    if (hours > 0) {
        
        if (sec < 10) {
            seconds = [NSString stringWithFormat:@"0%i",sec];
        }else{
            seconds = [NSString stringWithFormat:@"%i",sec];
        }
        
        if (min < 10) {
            minutes= [NSString stringWithFormat:@"0%i:%@",min,seconds];
        }else{
            minutes = [NSString stringWithFormat:@"%i:%@",min,seconds];
        }
        
        if (hours > 1) {
            return [NSString stringWithFormat:@"%i:%@",hours,minutes];
        }else{
            return [NSString stringWithFormat:@"%i:%@",hours,minutes];
        }
        
    }else{
        
        //If less then 0 remove negative from seconds
        if (sec < 0) {
            sec = sec * -1;
        }
        
        if (sec < 10) {
            seconds = [NSString stringWithFormat:@"0%i",sec];
        }else{
            seconds = [NSString stringWithFormat:@"%i",sec];
        }
        
        if (min > 0) {
            return [NSString stringWithFormat:@"%i:%@",min,seconds];
        }else{
            return [NSString stringWithFormat:@"%i:%@",min,seconds];
        }
        
    }
    
}

/**
 Method to compare the given time and current time
 
 @param time given time
 
 @return true or faulse
 */
+(BOOL)isStillAvailable:(double)time{
    
    int currTime = [self GetCurrentTime];
    
    if (time > currTime) {
        return TRUE;
    }else{
        return FALSE;
    }
}

/**
 Method to compare the given time and current time
 
 @param time given time
 
 @return true or faulse
 */
+(BOOL)isTime:(double)time lessThen:(double)days{
    
    //Multiply the quantity of seconds for days and sum to end day
    //86400 = quantity of seconds in a day
    time += days * 86400;
    
    if (time > [self GetCurrentTime]) {
        return true;
    }else{
        return false;
    }
}

+(BOOL)isTime:(double)time1 before:(double)time2{
    if (time1 > time2) {
        return true;
    }else{
        return false;
    }
}

+(BOOL)isTimeBeforeNow:(double)time{
    NSLog(@"%f",time);
    NSLog(@"%f",[[NSDate date] timeIntervalSince1970]);
    if (time > [[NSDate date] timeIntervalSince1970]) {
        return true;
    }else{
        return false;
    }
}

@end
