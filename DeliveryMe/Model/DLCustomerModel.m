//
//  DLCustomerModel.m
//  Delivery
//
//  Created by iMac on 14/4/17.
//  Copyright © 2017 Braziosdev. All rights reserved.
//

#import "DLCustomerModel.h"

@implementation DLCustomerModel

-(id)initObjectWithDictionary:(NSDictionary *)customerInfo{

    [self setCustomerId:[CKUtility getIntValueForKey:[customerInfo valueForKey:@"customerId"]]];
    [self setRouteMapId:[CKUtility getIntValueForKey:[customerInfo valueForKey:@"routeMapId"]]];
    [self setOrder:[CKUtility getIntValueForKey:[customerInfo valueForKey:@"order"]]];
    [self setName:[CKUtility getStringvalueForKey:[customerInfo valueForKey:@"name"]]];
    [self setMobile:[CKUtility getStringvalueForKey:[customerInfo valueForKey:@"mobile"]]];
    [self setAddress:[CKUtility getStringvalueForKey:[customerInfo valueForKey:@"address"]]];
    [self setNote:[CKUtility getStringvalueForKey:[customerInfo valueForKey:@"note"]]];
    [self setLat:[CKUtility getDoubleValueForKey:[customerInfo valueForKey:@"lat"]]];
    [self setLng:[CKUtility getDoubleValueForKey:[customerInfo valueForKey:@"lng"]]];
    
    [self setLocation:[[CLLocation alloc] initWithLatitude:self.lat longitude:self.lng]];
    
    return self;
}


@end
