//
//  SBUtility.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 3/12/2015.
//  Copyright © 2015 com.alerton.com. All rights reserved.
//

#import "CKUtility.h"

static CKUtility *shareInstance;

@implementation CKUtility {
    
    UIViewController *mainController;
    
}

+(CKUtility *)shareInstance{
 
    if (shareInstance == NULL) {
        shareInstance = [[CKUtility alloc] init];
        return shareInstance;
    }
    
    return shareInstance;
}

+(void)callNumber:(NSString *)phone{
    
    @try {
        //Call user
        NSString *phoneNumber = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",phoneNumber]]];
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

+(void)callEmergencyNumber:(NSString *)phone{
    
    @try {
        //Call user
        NSString *phoneNumber = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",phoneNumber]]];
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}


-(void)smsNumber:(NSString *)phone aboveController:(UIViewController *)viewController{
    
    mainController = viewController;
    NSString *phoneNumber = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText]){
        //[controller setBody:MSG_SMS_USER_SAFE];
        [controller setRecipients:[NSArray arrayWithObject:phoneNumber]];
        [controller setMessageComposeDelegate:self];
        [mainController presentViewController:controller animated:YES completion:nil];
    }
    
}

-(void)emailUser:(NSString *)email aboveController:(UIViewController *)viewController{
    
    mainController = viewController;
    
    @try {
        MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
        [composer setMailComposeDelegate:self];
        
        if ([MFMailComposeViewController canSendMail]) {
            [composer setToRecipients:[NSArray arrayWithObjects:email, nil]];
            [composer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [mainController presentViewController:composer animated:YES completion:nil];
            
        }else{
            
            [[[UIAlertView alloc] initWithTitle:@"error" message:MSG_SEND_EMAIL_ERROR delegate:nil cancelButtonTitle:@"dismiss" otherButtonTitles:nil] show];
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

+(void)openUrl:(NSString *)url{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    
}

+(void)getDirectionForLat:(CLLocation *)location{
    
    //Validate location before getting direction
    if (location.coordinate.latitude != 0 && location.coordinate.longitude != 0) {
     
        if (CKPerson.directionsOption == 1) {
            
            NSString *url = [NSString stringWithFormat:@"comgooglemaps://?q=%f,%f",location.coordinate.latitude,location.coordinate.longitude];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
        }else{
        
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude) addressDictionary:nil];
            MKMapItem *destination = [[MKMapItem alloc] initWithPlacemark:placemark];
            
            //This is for driving
            [MKMapItem openMapsWithItems:@[destination] launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving}];
            
            //This is for walking
            //[MKMapItem openMapsWithItems:@[destination] launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking}];
            
        }
        
    }else{
        [[[UIAlertView alloc] initWithTitle:nil message:MSG_LOCATION_USER_INVALID delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

+(void)getDirectionForString:(NSString *)address andCoordinate:(CLLocation *)location{
    
    //Validate location before getting direction
    if (address.length != 0) {
        
        if (CKPerson.directionsOption == 1) {
            
            NSString *url = [NSString stringWithFormat:@"comgooglemaps://?q=%@",[address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
        }else{
            
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude) addressDictionary:nil];
            MKMapItem *destination = [[MKMapItem alloc] initWithPlacemark:placemark];
            
            //This is for driving
            [MKMapItem openMapsWithItems:@[destination] launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving}];
            
            //This is for walking
            //[MKMapItem openMapsWithItems:@[destination] launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking}];
            
        }
        
    }else{
        [[[UIAlertView alloc] initWithTitle:nil message:MSG_LOCATION_USER_INVALID delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}


-(void)takePictureAbove:(id)viewController{
    
    mainController = viewController;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:viewController];
    [picker setAllowsEditing:TRUE];
    [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [viewController presentViewController:picker animated:YES completion:NULL];
}

-(void)choosePictureAbove:(id)viewController{

    mainController = viewController;
    
    //Choose picture
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:viewController];
    [picker setAllowsEditing:TRUE];
    [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [viewController presentViewController:picker animated:YES completion:NULL];
}


-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            [[[UIAlertView alloc] initWithTitle:nil message:MSG_SMS_SEND_FAIL delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            break;
        case MessageComposeResultSent:
            [[[UIAlertView alloc] initWithTitle:nil message:MSG_SMS_SEND_SUCCESS delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            break;
        default:
            break;
    }
    
    [mainController dismissViewControllerAnimated:YES completion:nil];
}


-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    if(error) {
        [[[UIAlertView alloc] initWithTitle:@"error" message:[NSString stringWithFormat:@"error %@", [error description]] delegate:nil cancelButtonTitle:@"dismiss" otherButtonTitles:nil] show];
        
        [mainController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [mainController dismissViewControllerAnimated:YES completion:nil];
    }
}

+(void)callNumberWithPrompt:(NSString *)phone{
    
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"Can't call %@, you are using a simulator",phone);
#else
    @try {
        //Call user
        NSString *phoneNumber = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",phoneNumber]]];
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
#endif
    
}

-(void)smsNumber:(NSString *)phone withMessage:(NSString *)message aboveController:(UIViewController *)viewController{
    
    mainController = viewController;
    NSString *phoneNumber = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
    
    if([MFMessageComposeViewController canSendText]){
        [controller setBody:message];
        [controller setRecipients:[NSArray arrayWithObject:phoneNumber]];
        [controller setMessageComposeDelegate:self];
        [viewController presentViewController:controller animated:YES completion:nil];
    }
    
}

/**
 *  Method created to simplify how to display alert and making sure it run on the main thread
 *
 *  @param message  Message to be loaded on the alert
 *  @param delegate class that will handle callback
 *  @param cancel   String wor the cancel button
 */
+(UIAlertView *)AlertMessageWithOnebutton:(NSString *)message title:(NSString *)title withDelegate:(UIViewController *)delegate withCancel:(NSString *)cancel{
    return [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancel otherButtonTitles:nil];
}

/**
 *  Method created to simplify how to display alert and making sure it run on the main thread
 *
 *  @param message  Message to be loaded on the alert
 *  @param delegate class that will handle callback
 *  @param cancel   String wor the cancel button
 */
+(void)ShowAlertMessageWithOnebutton:(NSString *)message title:(NSString *)title withDelegate:(UIViewController *)delegate withCancel:(NSString *)cancel{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancel otherButtonTitles:nil] show];
    });
}

/**
 *  Method created to simplify how to display alert and making sure it run on the main thread
 *
 *  @param message  Message to be loaded on the alert
 *  @param delegate class that will handle callback
 *  @param cancel   String for the cancel button
 *  @param proceed  String for the positive button
 */
+(void)ShowAlertMessageWithTwobutton:(NSString *)message title:(NSString *)title withDelegate:(UIViewController *)delegate withCancel:(NSString *)cancel andProceed:(NSString *)proceed{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancel otherButtonTitles:proceed,nil] show];
    });
}

/**
 *  Method to show alert message on main thread
 *
 *  @param message  Message to be loaded on the alert
 *  @param delegate class that will handle callback
 *  @param cancel   String for the cancel button
 *  @param proceed  String for the positive button
 */
+(void)ShowAlertMessageOnMainThread:(UIAlertView *)alert{
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert show];
    });
}

+(BOOL)validateEmail:(NSString *)emailStr{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

+(NSData *)getJsonEncodedData:(NSDictionary *)value{
    
    //Transform array into json format
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:value options:kNilOptions error:&error];
    
    if (!error) {
        return jsonData;
    }else{
        NSLog(@"SBUtility -- Error encoding json data");
        return nil;
    }
    
}

/**
 Method to resize image to be uploaded
 
 @param image   the image
 @param newSize new size
 
 @return new image
 */
+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(NSString *)getStringvalueForKey:(NSString *)value{
    @try {
        if ([value isKindOfClass:[NSNull class]] || value == nil) {
            return @"";
        }
        return value;
    }
    @catch (NSException *exception) {
        return @"";
    }
}

+(double)getDoubleValueForKey:(NSString *)value{
    @try {
        if ([value isKindOfClass:[NSNull class]] || value == nil) {
            return 0;
        }
        return [value doubleValue];
    }
    @catch (NSException *exception) {
        return 0;
    }
}

+(int)getIntValueForKey:(NSString *)value{
    @try {
        if ([value isKindOfClass:[NSNull class]] || value == nil) {
            return 0;
        }
        return [value intValue];
    }
    @catch (NSException *exception) {
        return 0;
    }
}

+(BOOL)getBooleanValueForKey:(NSString *)value{
    @try {
        if ([value isKindOfClass:[NSNull class]] || value == nil) {
            return false;
        }
        return [value boolValue];
    }
    @catch (NSException *exception) {
        return false;
    }
}

+(NSArray *)getArrayValueForKey:(NSArray *)value{
    if ([value isKindOfClass:[NSNull class]]) {
        return nil;
    }
    return value;
}

@end
