//
//  SBCoreLocation.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 27/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKCoreLocation.h"

@implementation CKCoreLocation{
    CLLocationManager *locationManager;
}

static CKCoreLocation *sharedInstance = nil;

+(CKCoreLocation *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

-(void)initShareLocationManager{
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    if ([self IsLocationAuthorized]) {
        //[locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        [locationManager startMonitoringSignificantLocationChanges];
        [CKPerson setLocation:[[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude]];
        NSLog(@"Initiating location: %@",CKPerson.location);
    }else{
        
        NSLog(@"Request location authorization");
        [locationManager requestWhenInUseAuthorization];
        
    }
}

#pragma -mark CORELOCATION Own delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [CKPerson setLocation:[[CLLocation alloc] initWithLatitude:[[locations lastObject] coordinate].latitude longitude:[[locations lastObject] coordinate].longitude]];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Failed: %@",error);
}

#pragma -mark GEOFENCE
-(void)startMonitorExitGeofence{
    
    CLCircularRegion *region;
    double distance = [CKPerson.location distanceFromLocation:DLRoute.location];
    
    if (locationManager == NULL) {
        [self initShareLocationManager];
    }
    
    //User distance is greater than the site + allowed radius, increase the geofence to the accepted radius
    if ((distance > (200 + ALLOWED_RADIUS))) {
        region = [[CLCircularRegion alloc] initWithCenter:DLRoute.location.coordinate radius:200 + ACCEPTED_RADIUS identifier:@"geoLocationExit"];
    }else{
        region = [[CLCircularRegion alloc] initWithCenter:DLRoute.location.coordinate radius:200 + ALLOWED_RADIUS identifier:@"geoLocationExit"];
    }
    
    [locationManager startMonitoringForRegion:region];
    
}

-(void)startMonitorEnterGeofence{
    
    if (locationManager == NULL) {
        [self initShareLocationManager];
    }
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:DLRoute.location.coordinate radius:200 + ALLOWED_RADIUS identifier:@"geoLocationEnter"];
    [locationManager startMonitoringForRegion:region];
    
}

////-(void)igLocationManager:(IGLocationManager *)manager didExitRegion:(IGRegion *)region{
//-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
//    
//    if (DLRoute.isCheckin) {
//        
//        //[self stopMonitorGeofence];
//        
//        SBSite.leftGeofence = TRUE;
//        [SBSiteModel UpdateSiteInfo];
//        
//        //Build SBNotification object and delivery
//        SBNotification *notification = [[SBNotification alloc] init];
//        [notification setAlert:[NSString stringWithFormat:MSG_LOCAL_NOTF_GEOFENCE_EXIT,SBUser.firstName]];
//        [notification setFireTime:0];
//        [notification setNotificationType:SBActionNotificationTypeGeofence];
//        [notification setSound:kSound_hb];
//        [notification setDelivered:FALSE];
//        [notification setTriggerOnce:TRUE];
//        [notification setBadge:[[UIApplication sharedApplication]applicationIconBadgeNumber] + 1];
//        [notification setIdentifier:NOTIF_INTERACTIVE_GEOFENCE];
//        [SBNotification DeliveryNotification:notification];
//        
//        //Save that user left the geofence to db without the location - by Trevor on meetin 29/03/16
//        [self RequestSaveLocation];
//        
//        //Trigger silent geofence to show on user screen
//        //[self triggerExitGeofenceSilence:region];
//        
//    }
//    
//}
//
////-(void)igLocationManager:(IGLocationManager *)manager didEnterRegion:(IGRegion *)region{
//-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
//    
//    [SBLog log:[NSString stringWithFormat:@"SBCoreLocation enter geofence ==> site lat: %f - lng: %f - radius: %d",SBSite.location.coordinate.latitude,SBSite.location.coordinate.longitude,SBSite.radius]];
//    
//    if (!SBSite.isCheckin) {
//        
//        //[self stopMonitorGeofence];
//        
//        //Build SBNotification object and delivery
//        SBNotification *notification = [[SBNotification alloc] init];
//        [notification setAlert:[NSString stringWithFormat:MSG_LOCAL_NOTF_GEOFENCE_ENTER,SBUser.firstName,SBSite.siteName]];
//        [notification setFireTime:0];
//        [notification setNotificationType:SBActionNotificationTypeGeofence];
//        [notification setSound:kSound_hb];
//        [notification setDelivered:FALSE];
//        [notification setTriggerOnce:TRUE];
//        [notification setBadge:[[UIApplication sharedApplication]applicationIconBadgeNumber] + 1];
//        [notification setIdentifier:NOTIF_INTERACTIVE_GEOFENCE];
//        [SBNotification DeliveryNotification:notification];
//        
//        //Trigger silent geofence to show on user screen
//        //[self triggerEnterGeofenceSilence:region];
//    }
//    
//}

-(void)stopMonitorGeofence{
    
    if (locationManager == NULL) {
        [self initShareLocationManager];
    }
    
    // stop monitoring for any and all current regions
    for (CLRegion *region in [[locationManager monitoredRegions] allObjects]) {
        [locationManager stopMonitoringForRegion:region];
    }
    
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error{
    NSLog(@"Region: %@ - failed: %@",region,error);
}

/**
 *  Method will validate if user had giving location persimission to fetch location and call the main class
 */
-(void)GetUserLocationPermission{
    
    //Check if user is running on iOS 8 or above
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
            if (status == kCLAuthorizationStatusAuthorizedAlways ) {
                [self.delegate UserLocationIsEnable:TRUE andAlways:TRUE];
            }else{
                [self.delegate UserLocationIsEnable:TRUE andAlways:FALSE];
            }
        }else{
            
            [self initShareLocationManager];
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun]) {
                [self.delegate UserLocationIsEnable:FALSE andAlways:FALSE];
            }
        }
    }else{
        
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusAuthorized) {
            [self.delegate UserLocationIsEnable:TRUE andAlways:TRUE];
        }else{
            
            [self initShareLocationManager];
            
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun]) {
                [self.delegate UserLocationIsEnable:FALSE andAlways:FALSE];
            }
        }
        
    }
    
}


/**
 *  Method that will handle user battery level. It will send push if user battery level are:
 *  10% - Alert user only
 *  8% - Alert user and team leaders
 *  Battery level bellow 8% will stop the background track to avoid battery consumption
 */
-(int)GetBatteryLevel{
    
# if TARGET_IPHONE_SIMULATOR
    return 50;
#endif
    
    //Get battery level
    return [[UIDevice currentDevice] batteryLevel] * 100;
    //battery = 9;
    
}

/**
 *  Remove background track command
 */
-(void)StopShareLocationManager{
    
    //[IGLocationManager initWithDelegate:self secretAPIKey:@"b4ef183317dbb4b101ac3c7533066b2c"];
    //[IGLocationManager stopUpdatingLocation];
    [locationManager stopUpdatingHeading];
}

/**
 * Method will stop and start the location service again to force retrieve correct location
 **/
-(void)RebuildShareLocationManager{
    [self StopShareLocationManager];
    [self initShareLocationManager];
}

+(BOOL)isValidLocation:(CLLocation *)location{
    NSLog(@"SBCoreLocation - isValidLocation %@",location);
    if (location.coordinate.latitude == 0 || location.coordinate.longitude == 0) {
        return false;
    }else{
        return true;
    }
}

-(BOOL)IsLocationAuthorized{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
            return TRUE;
        }else{
            return FALSE;
        }
    }else{
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusAuthorized) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

@end
