//
//  SBCoreLocation.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 27/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol CKCoreLocationDelegate <NSObject>

@optional
-(void)DidFinishGettingUserLocationChange:(BOOL)locationChanged;
-(void)UserLocationIsEnable:(BOOL)locationEnable andAlways:(BOOL)locationAways;
-(void)DidFinishWithError:(NSString *)error;

@end

@interface CKCoreLocation : NSObject <CLLocationManagerDelegate>

@property (nonatomic, retain) id <CKCoreLocationDelegate> delegate;

+(CKCoreLocation *)sharedInstance;
-(void)initShareLocationManager;
-(void)StopShareLocationManager;
-(void)RebuildShareLocationManager;
-(void)GetUserLocationPermission;
+(BOOL)isValidLocation:(CLLocation *)location;

//Geofence
//-(void)startLocationManager;
-(void)startMonitorExitGeofence;
-(void)startMonitorEnterGeofence;
-(void)stopMonitorGeofence;


@end
