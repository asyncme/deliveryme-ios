//
//  SBUtility.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 3/12/2015.
//  Copyright © 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

@interface CKUtility : NSObject <MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

+(CKUtility *)shareInstance;
+(NSString *)getStringvalueForKey:(NSString *)value;
+(double)getDoubleValueForKey:(NSString *)value;
+(int)getIntValueForKey:(NSString *)value;
+(BOOL)getBooleanValueForKey:(NSString *)value;

+(void)callNumber:(NSString *)phone;
+(void)callEmergencyNumber:(NSString *)phone;
-(void)smsNumber:(NSString *)phone aboveController:(UIViewController *)viewController;
-(void)emailUser:(NSString *)email aboveController:(UIViewController *)viewController;
+(void)openUrl:(NSString *)url;
+(void)getDirectionForLat:(CLLocation *)location;
+(void)getDirectionForString:(NSString *)address andCoordinate:(CLLocation *)location;
-(void)takePictureAbove:(UIViewController *)viewController;
-(void)choosePictureAbove:(UIViewController *)viewController;
+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;

+(NSArray *)getArrayValueForKey:(NSString *)value;
+(NSData *)getJsonEncodedData:(NSDictionary *)value;

+(void)callNumberWithPrompt:(NSString *)phone;
-(void)smsNumber:(NSString *)phone withMessage:(NSString *)message aboveController:(UIViewController *)viewController;
+(BOOL)validateEmail:(NSString *)emailStr;

+(UIAlertView *)AlertMessageWithOnebutton:(NSString *)message title:(NSString *)title withDelegate:(UIViewController *)delegate withCancel:(NSString *)cancel;
+(void)ShowAlertMessageWithTwobutton:(NSString *)message title:(NSString *)title withDelegate:(UIViewController *)delegate withCancel:(NSString *)cancel andProceed:(NSString *)proceed;
+(void)ShowAlertMessageWithOnebutton:(NSString *)message title:(NSString *)title withDelegate:(UIViewController *)delegate withCancel:(NSString *)cancel;
+(void)ShowAlertMessageOnMainThread:(UIAlertView *)alert;

@end
