//
//  SBSiteModel.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 26/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLRouteModel : NSObject

@property (nonatomic, copy) NSString *name,*timezone;
@property (nonatomic, copy) NSDictionary *customers;
@property double time;
@property (nonatomic, retain) CLLocation *location;
@property int routeId,companyId,personId,routeOrder,qtdCustomers;

@property (nonatomic, copy) NSMutableArray *arrayCustomers;

-(id)initObjectWithDictionary:(NSDictionary *)routeInfo;

@end
