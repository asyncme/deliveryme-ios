//
//  SBEntityModel.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 23/02/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKCompanyModel.h"

@implementation CKCompanyModel

static CKCompanyModel *shareInstance;

+(CKCompanyModel *)shareInstance{
    if (shareInstance == NULL) {
        shareInstance = [[CKCompanyModel alloc] init];
        return shareInstance;
    }
    
    return shareInstance;
}

-(id)initObjectWithDictionary:(NSDictionary *)companyInfo{
    
    @try {
        
        self.companyId = [CKUtility getIntValueForKey:[companyInfo valueForKey:@"companyId"]];
        self.adminId = [CKUtility getIntValueForKey:[companyInfo valueForKey:@"adminId"]];
        self.companyName = [CKUtility getStringvalueForKey:[companyInfo valueForKey:@"companyName"]];
                
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:companyInfo];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setValue:dataObj forKey:kCKCompanyModel];
        }else{
            //Clockout user
            [CKMain TakeUserToLogin];
        }
        
        return self;
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}


-(void)initSBEntityFromMemory{
    
    @try {

        NSData *archaviedData = [[NSUserDefaults standardUserDefaults] valueForKey:kCKCompanyModel];
        
        if (archaviedData) {
            NSMutableDictionary *arraySBEntity = [NSKeyedUnarchiver unarchiveObjectWithData:archaviedData];
            if ([arraySBEntity count] != 0) {
                CKCompany = [self initObjectWithDictionary:arraySBEntity];
            }
        }else{
            //Support for users that are migrating from the previous version
            CKCompany = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKCompanyModel]];
        }

    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        //Support for users that are migrating from the previous version
        NSDictionary *entityInfo = [[NSUserDefaults standardUserDefaults] valueForKey:kCKCompanyModel];
        if (entityInfo) {
            CKCompany = [self initObjectWithDictionary:entityInfo];
        }
        
    }
}

+(void)UpdateCompanyInfo{
    
    @try {
        
        NSMutableDictionary *updateEntity = [[NSMutableDictionary alloc] init];
        [updateEntity setValue:[NSNumber numberWithInt:CKCompany.companyId] forKey:@"companyId"];
        [updateEntity setValue:[NSNumber numberWithInt:CKCompany.adminId] forKey:@"adminId"];
        [updateEntity setValue:CKCompany.companyName forKey:@"companyName"];
        NSLog(@"----> Updated Entity model <----");
        
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:updateEntity];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setValue:dataObj forKey:kCKCompanyModel];
        }else{
            //Clockout user
            [CKMain TakeUserToLogin];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }

}

@end
