//
//  SBAnimation.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKAnimation.h"

@implementation CKAnimation

static CKAnimation *sharedInstance = nil;

+(CKAnimation *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

/**
 Method to create the background image blur
 
 @return blur image
 */
+(UIImage *)CreateBlurBackground:(UIView *)copyView andEffect:(SBAnimationStyle)style{
    
    UIImage *image;
    
    @try {
        CGSize size = CGSizeMake(copyView.frame.size.width,copyView.frame.size.height);
        UIGraphicsBeginImageContext(size);
        [copyView drawViewHierarchyInRect:CGRectMake(0, 0, size.width, size.height) afterScreenUpdates:NO];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    if (image) {
        if (style == SBAnimationStyleLight) {
            return [image applyLightEffect];
        }else if(style == SBAnimationStyleDark){
            return [image applyDarkEffect];
        }else if(style == SBAnimationStyleWhiter){
            return [image applyWhiterEffect];
        }else{
            return [image applyLightEffect];
        }
    }else{
        return nil;
    }
}


/**
 Method to identify the top view and add the loading to it
 */
+(UIViewController *)showAnimateView:(NSString *)viewName onTopOf:(UIViewController *)view{
    
    UIViewController *popUpView = [[UIStoryboard storyboardWithName:kStoryBoardMain bundle:nil] instantiateViewControllerWithIdentifier:viewName];
    
    if (popUpView) {
        //[view presentPopupViewController:popUpView animationType:MJPopupViewAnimationFade];
    }
    
    return popUpView;
}

/**
 Method to remove the loading from view
 */
+(void)hideAnimateView:(UIViewController *)view{
    //[view dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

+(void)FlashView:(UIView *)view{
    
    [view setBackgroundColor:[UIColor redColor]];
    CABasicAnimation *flashAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    flashAnimation.duration = 1.0;
    flashAnimation.repeatCount = HUGE_VALF;
    flashAnimation.autoreverses = YES;
    flashAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    flashAnimation.toValue = [NSNumber numberWithFloat:0.0];
    [view.layer addAnimation:flashAnimation forKey:@"animateOpacity"];
    
}

+(void)RemoveFlashFromView:(UIView *)aview withColor:(UIColor *)aColor{
    
    [aview.layer removeAllAnimations];
    [aview setBackgroundColor:aColor];
    
}

/**
 Method to resize the font size depending on the size of the question
 
 @param str         text to check
 @param currentSize current text size
 @param labelRect   the label to feet the text
 
 @return size of the text
 */
+(CGFloat)TextSize:(NSString *)str withFontSize:(CGFloat)currentSize forLabel:(CGRect)labelRect{
    CGFloat fontSize = 12;
    
    while (fontSize > 0.0){
        CGSize size = [str sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
        if (size.width <= labelRect.size.width){
            break;
        }else{
            fontSize -= 1.0;
        }
    }
    
    return fontSize;
}

@end
