//
//  SBColor.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 6/07/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKColor.h"

@implementation CKColor

+(UIColor *)CKLightBlueColor{
    return [UIColor colorWithRed:169.0f/255.0f green:204.0f/255.0f blue:227.0f/255.0f alpha:1];
}

+(UIColor *)CKDefaultColor{
    return [UIColor colorWithRed:26.0f/255.0f green:82.0f/255.0f blue:118.0f/255.0f alpha:1];
}

+(UIColor *)CKLightGrayColor{
    return [UIColor colorWithRed:213.0f/255.0f green:216.0f/255.0f blue:220.0f/255.0f alpha:1];
}

+(UIColor *)CKVeryLightGrayColor{
    return [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1];
}

+(UIColor *)CKOrangeColor{
    return [UIColor colorWithRed:235.0f/255.0f green:95.0f/255.0f blue:49.0f/255.0f alpha:1];
}

+(UIColor *)CKGreenColor{
    return [UIColor colorWithRed:35.0f/255.0f green:155.0f/255.0f blue:86.0f/255.0f alpha:1];
}



+(UIColor *)CKYellowColor{
    return [UIColor colorWithRed:250.0f/255.0f green:209.0f/255.0f blue:21.0f/255.0f alpha:1];
}

+(UIColor *)CKBannerColor{
    return [UIColor colorWithRed:255.0f/255.0f green:173.0f/255.0f blue:23.0f/255.0f alpha:1];
}

+(UIColor *)CKLightRedColor{
    return [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.4];
}

@end
