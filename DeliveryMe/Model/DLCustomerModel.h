//
//  DLCustomerModel.h
//  Delivery
//
//  Created by iMac on 14/4/17.
//  Copyright © 2017 Braziosdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLCustomerModel : NSObject

@property (nonatomic, copy) NSString *name,*mobile,*address,*note;
@property (nonatomic, copy) CLLocation *location;
@property int customerId,routeMapId,order;
@property double lat,lng;

-(id)initObjectWithDictionary:(NSDictionary *)customerInfo;

@end
