//
//  CKMenuViewController.h
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLMenuViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView *imgBg;
@property (nonatomic, strong) UIImage *imgReceive;
@property (nonatomic, weak) IBOutlet UIButton *btnSchedule,*btnProfile,*btnCancel,*btnExit;

-(IBAction)Schedule:(id)sender;
-(IBAction)Profile:(id)sender;
-(IBAction)Exit:(id)sender;

//Bottom Menu
-(IBAction)Cancel:(id)sender;


@end
