//
//  CKMenuViewController.m
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import "DLMenuViewController.h"

@interface DLMenuViewController ()

@end

@implementation DLMenuViewController

@synthesize imgBg,imgReceive;
@synthesize btnSchedule,btnProfile,btnExit;

- (void)viewDidLoad {
    [super viewDidLoad];
    [imgBg setImage:imgReceive];
    if (CKPerson.personId == 0) {
        [btnExit setTitle:@"Exit Demo" forState:UIControlStateNormal];
    }else{
        [btnExit setTitle:@"Log out" forState:UIControlStateNormal];
    }
}

-(IBAction)Schedule:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"ScheduleView"]];
}

-(IBAction)Profile:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"ProfileView"]];
}

-(IBAction)Cancel:(id)sender{
    [self dismissView];
}

-(IBAction)Exit:(id)sender{
    [[CKLoginModel alloc] LogOffUserWithAccessDenied:self];
}

-(void)presentView:(UIViewController *)view{
    [view setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
