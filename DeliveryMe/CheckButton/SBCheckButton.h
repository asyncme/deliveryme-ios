//
//  SBCheckButton.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/06/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>

//User access level
typedef enum{
    SBCheckButtonStyleLight=0,
    SBCheckButtonStyleDark
}SBCheckButtonType;

@protocol SBCheckButtonDelegate <NSObject>

@optional
-(void)DidTapOnCheckButton:(BOOL)checked;

@end

@interface SBCheckButton : UIControl

@property (nonatomic, retain) id <SBCheckButtonDelegate> delegate;
@property (nonatomic, assign) BOOL checked;
@property (nonatomic, assign) SBCheckButtonType SBCheckButtonStyle;

-(id)initWithFrame:(CGRect)frame andStype:(SBCheckButtonType)SBCheckType selected:(BOOL)selected;
-(void)Check;
-(void)CheckWithDelegate;


@end
