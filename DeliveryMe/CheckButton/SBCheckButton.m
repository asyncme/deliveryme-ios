//
//  SBCheckButton.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/06/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "SBCheckButton.h"

#define SBCheckX 26
#define SBCheckY 20

@interface SBCheckButton(){
    UIImageView *imgButton;
}

@end

@implementation SBCheckButton

@synthesize checked,SBCheckButtonStyle;

-(id)initWithFrame:(CGRect)frame andStype:(SBCheckButtonType)SBCheckType selected:(BOOL)selected{

    self = [super initWithFrame:frame];
    
    if(self){
        
        imgButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SBCheckX, SBCheckY)];
        checked = selected;
        SBCheckButtonStyle = SBCheckType;
        
        if (SBCheckType == SBCheckButtonStyleDark) {
            
            if (selected) {
                [imgButton setImage:[UIImage imageNamed:@"btn_checked_blue"]];
            }else{
                [imgButton setImage:[UIImage imageNamed:@"btn_unchecked_blue"]];
            }
        }else{
            if (selected) {
                [imgButton setImage:[UIImage imageNamed:@"btn_checked"]];
            }else{
                [imgButton setImage:[UIImage imageNamed:@"btn_unchecked"]];
            }
        }
        
        
        [self addSubview:imgButton];
    }
    
    return self;
    
}

-(void)Check{
    
    if (SBCheckButtonStyle == SBCheckButtonStyleDark) {
        
        if (checked) {
            checked = FALSE;
            [imgButton setImage:[UIImage imageNamed:@"btn_unchecked_blue"]];
        }else{
            checked = TRUE;
            [imgButton setImage:[UIImage imageNamed:@"btn_checked_blue"]];
        }
    }else{
        if (checked) {
            checked = FALSE;
            [imgButton setImage:[UIImage imageNamed:@"btn_unchecked"]];
        }else{
            checked = TRUE;
            [imgButton setImage:[UIImage imageNamed:@"btn_checked"]];
        }
    }
}

-(void)CheckWithDelegate{
    
    if (checked) {
        checked = FALSE;
        [imgButton setImage:[UIImage imageNamed:@"btn_unchecked"]];
    }else{
        checked = TRUE;
        [imgButton setImage:[UIImage imageNamed:@"btn_checked"]];
    }
    
    [self.delegate DidTapOnCheckButton:checked];
    
}

@end
