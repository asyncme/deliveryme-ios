//
//  BlinkViewController.m
//  FootyFormPredictions
//
//  Created by Paulo Goncalves on 25/03/2014.
//  Copyright (c) 2014 Anythingnet. All rights reserved.
//

#import "CKViewController.h"

@interface CKViewController (){
    UIImageView *heart;
}

@end

@implementation CKViewController

static CKViewController *sharedInstance = nil;

+(CKViewController *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [UIView animateWithDuration:0.2 animations:^void() {
        self.view.backgroundColor = [[CKColor CKDefaultColor] colorWithAlphaComponent:1];
        self.view.layer.cornerRadius = 5;
        self.view.layer.shadowOpacity = 0.4;
        self.view.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        [self.view setFrame:CGRectMake(130, 250, 100, 100)];
    }];
    
}

-(void)viewWillLayoutSubviews{
    
    NSMutableArray *clockingArray = [[NSMutableArray alloc] init];
    for (int i = 1; i<21; i++) {
        [clockingArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loading_%i",i]]];
    }
    
    UIImageView *imgHeart = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
    [imgHeart setAnimationImages:clockingArray];
    [imgHeart setAnimationDuration:1.5];
    [imgHeart setAnimationRepeatCount:0];
    [imgHeart startAnimating];
    [self.view addSubview:imgHeart];
}

-(void)viewDidAppear:(BOOL)animated{
    
    
}

-(void)showClockInView:(UIView *)aView{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [aView addSubview:self.view];
        
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0;
        [UIView animateWithDuration:0 animations:^{
            self.view.alpha = 1;
            self.view.transform = CGAffineTransformMakeScale(1, 1);
        }];
        
    });

}

-(void)removeClock{

    dispatch_async(dispatch_get_main_queue(), ^{
    
        if (self.view) {
            
            [UIView animateWithDuration:0 animations:^{
                self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
                self.view.alpha = 0.0;
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.view removeFromSuperview];
                }
            }];
            
        }
        
    });
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
