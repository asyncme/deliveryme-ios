//
//  LoginModel.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 14/01/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKLoginModel : NSObject <CLLocationManagerDelegate,CKCoreDelegate>

+(CKLoginModel *)sharedInstance;

-(void)LoginSucess:(NSDictionary *)dictionary;
-(void)RegisterDeviceToken:(NSData *)deviceToken;
-(void)LogOffUserWithAccessDenied:(UIViewController *)view;
-(void)RestoreSession;
-(void)BeginCKSession;
-(BOOL)UpdateAppVersion;

@end
