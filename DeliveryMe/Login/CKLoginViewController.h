//
//  ViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "DLDemoData.h"
#import "CKWebViewController.h"

@interface CKLoginViewController : UIViewController <UIAlertViewDelegate,UIApplicationDelegate,UITextFieldDelegate,CKCoreDelegate>

@property (nonatomic,weak) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic) IBOutlet UITextField *txtEmail;
@property (nonatomic) IBOutlet UITextField *txtPassword;
@property (nonatomic) IBOutlet UILabel *lblVersion;
@property (nonatomic) IBOutlet UIImageView *imgBg;

-(IBAction)Login:(id)sender;
//-(IBAction)Register:(id)sender;
-(IBAction)ForgetPassword:(id)sender;
-(IBAction)Background:(id)sender;
-(IBAction)Skip:(id)sender;

@end
