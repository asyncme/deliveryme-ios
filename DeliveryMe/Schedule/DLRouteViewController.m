//
//  UserConnectionViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "DLRouteViewController.h"

@interface DLRouteViewController (){
    CKCoreWebService *CK;
    CKCoreLocation *CKLocation;
    NSTimer *refreshRow;
}

@end

@implementation DLRouteViewController

@synthesize tblContent,routeModel;

- (void)viewDidLoad{
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
}

-(void)viewDidAppear:(BOOL)animated{
    if(routeModel.routeId == 0){
        
        // Load demo customer routes
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObject:[DLDemoData getDemoCustomer1]];
        [array addObject:[DLDemoData getDemoCustomer2]];
        [array addObject:[DLDemoData getDemoCustomer3]];
        [routeModel setArrayCustomers:[[self SortPlaceByOrder:array] mutableCopy]];
        
        [tblContent reloadData];
        
    }else{
        [self FetchData];
    }
    
}

-(void)DidFinishWithError:(NSString *)error{
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(IBAction)Back:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"ScheduleView"]];
}

-(IBAction)Start:(id)sender{
    
    //Set route as global and save to phone memory to reload when phone start
    routeModel.routeOrder = -1;
    DLRoute = routeModel;
    
    UIViewController *view = [[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"DeliveryView"];
    [view setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:view];
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)presentView:(UIViewController *)view{
    [view setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:view animated:YES completion:nil];
}

/**
 Method to fecth data
 */
-(void)FetchData{
    [[CKViewController sharedInstance] showClockInView:self.view];
    [self performSelectorInBackground:@selector(RequestRoutes) withObject:nil];
}

/**
 *  Method that will fetch all connections for the user
 */
-(void)RequestRoutes{
    
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&companyId=%i&routeId=%i",CKPerson.personId,CKPerson.token,CKCompany.companyId,routeModel.routeId];
    [CK CoreWebService:kRouteRoute withRequest:myRequestString];
}


#pragma -mark SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_RouteDetailSuccess) {
            
            [routeModel setCustomers:[dictionary valueForKey:@"customers"]];
            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
            
            for (NSMutableDictionary *dic in routeModel.customers) {
                [tempArray addObject:[[DLCustomerModel alloc] initObjectWithDictionary:dic]];
            }
            
            [routeModel setArrayCustomers:[[self SortPlaceByOrder:tempArray] mutableCopy]];

        }
        
        [tblContent reloadData];
        
    }else{
        
        [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
    }
    
    [[CKViewController sharedInstance] removeClock];
    
}

-(NSArray *)SortPlaceByOrder:(NSArray *)toSort{
    
    @try {
        
        NSSortDescriptor *sortDistance = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES comparator:^NSComparisonResult(id obj1, id obj2){
            DLCustomerModel *customer1 = (DLCustomerModel *)obj1;
            DLCustomerModel *customer2 = (DLCustomerModel *)obj2;
            
            return [[NSNumber numberWithDouble:customer1.order] compare:[NSNumber numberWithDouble:customer2.order]];
            
        }];
        
        return [toSort sortedArrayUsingDescriptors:@[sortDistance]];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        return nil;
    }
}


/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    [[CKViewController sharedInstance] removeClock];
    dispatch_async(dispatch_get_main_queue(), ^{
        [tblContent reloadData];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
}

#pragma -mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [routeModel.arrayCustomers count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *myIdentifier = @"cell";
    
    DLRouteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    if (cell == nil) {
        cell = [[DLRouteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
    //Validate if cell has been proper initialized before continue
    if (!cell) {
        return nil;
    }
    
    @try {
        
        DLCustomerModel *customer = [routeModel.arrayCustomers objectAtIndex:indexPath.row];
        
        [cell.lblCustomer setText:[NSString stringWithFormat:@"%@", customer.name]];
        [cell.lblPlaceName setText:[NSString stringWithFormat:@"Address: %@",customer.address]];
        [cell.lblNote setText:[NSString stringWithFormat:@"Note: %@",customer.note]];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tblContent deselectRowAtIndexPath:indexPath animated:NO];
    
    //Go to detail view of the route
    
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    });
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[CKViewController sharedInstance] removeClock];
}

@end
