//
//  DLDeliveryViewController.h
//  Delivery
//
//  Created by iMac on 14/4/17.
//  Copyright © 2017 Braziosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLDeliveryViewController : UIViewController <UIAlertViewDelegate,MKMapViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *lblName,*lblAddress,*lblNote,*lblMobile,*lblDistance,*lblTravelTime,*lblProgress;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UIProgressView *progress;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnPrevious,*btnNext;

-(IBAction)Next:(id)sender;
-(IBAction)Previous:(id)sender;
-(IBAction)Exit:(id)sender;
-(IBAction)Call:(id)sender;
-(IBAction)SMS:(id)sender;
-(IBAction)Direction:(id)sender;

@end
