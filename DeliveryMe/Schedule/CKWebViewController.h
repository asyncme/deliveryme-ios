//
//  SBWebViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/06/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, copy) NSURL *url;
@property (nonatomic, strong) IBOutlet UIWebView *webView;

-(IBAction)Close:(id)sender;

@end
