//
//  UserConnectionViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLRouteTableViewCell.h"
#import "DLDemoData.h"

@interface DLRouteViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,CKCoreDelegate,CKCoreLocationDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@property (nonatomic, retain) DLRouteModel *routeModel;

-(IBAction)Back:(id)sender;
-(IBAction)Start:(id)sender;

@end
