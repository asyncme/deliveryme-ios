//
//  DLDeliveryViewController.m
//  Delivery
//
//  Created by iMac on 14/4/17.
//  Copyright © 2017 Braziosdev. All rights reserved.
//

#import "DLDeliveryViewController.h"

@interface DLDeliveryViewController (){
    DLCustomerModel *currentCustomer;
    UIAlertView *confirmExit,*confirmDirection;
    NSString *travelTime;
}

@end

@implementation DLDeliveryViewController

@synthesize lblName,lblAddress,lblMobile,lblDistance,lblNote,lblTravelTime,lblProgress;
@synthesize btnNext,btnPrevious;
@synthesize progress;
@synthesize mapView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([DLRoute routeOrder] == -1){
        [DLRoute setRouteOrder:0];
        [self loadCustomerByOrder];
    }
    
    [self initMapView];
    
}

-(void)loadCustomerByOrder{
    
    currentCustomer = [DLRoute.arrayCustomers objectAtIndex:DLRoute.routeOrder];
    
    [self LoadMap];
    
    if (DLRoute.routeOrder == [DLRoute.arrayCustomers count] - 1) {
        UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(Exit:)];
        [self.navigationItem setRightBarButtonItem:btnDone];
    }else{
        UIBarButtonItem *btnN = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(Next:)];
        [self.navigationItem setRightBarButtonItem:btnN];
    }
    
    if (DLRoute.routeOrder == 0) {
        [self.navigationItem setLeftBarButtonItem:nil];
    }else{
        UIBarButtonItem *btnP = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStylePlain target:self action:@selector(Previous:)];
        [self.navigationItem setLeftBarButtonItem:btnP];
    }
    
    [self.lblName setText:[NSString stringWithFormat:@"Customer: %@",currentCustomer.name]];
    [self.lblAddress setText:[NSString stringWithFormat:@"Address: %@",currentCustomer.address]];
    [self.lblMobile setText:[NSString stringWithFormat:@"Mobile: %@",currentCustomer.mobile]];
    [self.lblNote setText:[NSString stringWithFormat:@"Note: %@",currentCustomer.note]];
    
    float percent = (((float)DLRoute.routeOrder + 1) / (float)[DLRoute.arrayCustomers count]);
    
    [progress setProgress:percent animated:YES];
    
    [lblProgress setText:[NSString stringWithFormat:@"%i out of %lu",DLRoute.routeOrder + 1,[DLRoute.arrayCustomers count]]];
    
}

-(IBAction)Next:(id)sender{
    
    [DLRoute setRouteOrder:DLRoute.routeOrder + 1];
    [self loadCustomerByOrder];
}

-(IBAction)Previous:(id)sender{
    
    [DLRoute setRouteOrder:DLRoute.routeOrder - 1];
    [self loadCustomerByOrder];
}

-(IBAction)Exit:(id)sender{
    
    confirmExit = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Are you sure you want to exit?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Exit", nil];
    [confirmExit show];
    
}

-(IBAction)Call:(id)sender{
    [CKUtility callNumber:currentCustomer.mobile];
}

-(IBAction)SMS:(id)sender{
    
    //Display predefined messages
    UIAlertController *actSMS = [UIAlertController alertControllerWithTitle:@"Please choose" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    NSArray *arrayName = [currentCustomer.name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *name = [arrayName objectAtIndex:0];
    
    [actSMS addAction:[UIAlertAction actionWithTitle:@"Arriving in ..." style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *message = [[NSString alloc] initWithFormat:@"Hi %@, I'will arrive in %@ with your ",name,travelTime];
        [[CKUtility shareInstance] smsNumber:currentCustomer.mobile withMessage:message aboveController:self];
    }]];
    
    [actSMS addAction:[UIAlertAction actionWithTitle:@"You're next" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *message = [[NSString alloc] initWithFormat:@"Hi %@, I'm on my way to your .",name];
        [[CKUtility shareInstance] smsNumber:currentCustomer.mobile withMessage:message aboveController:self];
    }]];
    
    [actSMS addAction:[UIAlertAction actionWithTitle:@"Arrived" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *message = [[NSString alloc] initWithFormat:@"Hi %@, I've just arrived.",name];
        [[CKUtility shareInstance] smsNumber:currentCustomer.mobile withMessage:message aboveController:self];
    }]];
    
    [actSMS addAction:[UIAlertAction actionWithTitle:@"Custom" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *message = [[NSString alloc] initWithFormat:@"Hi %@, ",name];
        [[CKUtility shareInstance] smsNumber:currentCustomer.mobile withMessage:message aboveController:self];
    }]];
    
    [actSMS addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [actSMS dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:actSMS animated:YES completion:nil];
    
}

-(IBAction)Direction:(id)sender{
    
    if (CKPerson.directionsOption == 0) {
        
        BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps:"]];
        
        if (canHandle) {
            confirmDirection = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"What would you like to use?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Google Maps",@"Apple Maps", nil];
            [confirmDirection show];
        }else{
            [CKUtility getDirectionForString:currentCustomer.address andCoordinate:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
            //[CKUtility getDirectionForLat:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
        }
        
    }else{
        [CKUtility getDirectionForString:currentCustomer.address andCoordinate:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
        //[CKUtility getDirectionForLat:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == confirmExit) {
        if (buttonIndex == 1) {
            [self Done];
        }
    }else if (alertView == confirmDirection){
        if (buttonIndex == 1) {
            CKPerson.directionsOption = 1;
            [CKPersonModel UpdateUserInfo];
            
            //[CKUtility getDirectionForLat:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
            [CKUtility getDirectionForString:currentCustomer.address andCoordinate:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
            
        }else if (buttonIndex == 2){
            CKPerson.directionsOption = 2;
            [CKPersonModel UpdateUserInfo];
            
            //[CKUtility getDirectionForLat:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
            [CKUtility getDirectionForString:currentCustomer.address andCoordinate:[[CLLocation alloc] initWithLatitude:currentCustomer.lat longitude:currentCustomer.lng]];
            
        }else{
            NSLog(@"Canceled");
        }
    }
}

-(void)Done{
    DLRoute = nil;
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"ScheduleView"]];
}

-(void)presentView:(UIViewController *)view{
    [view setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:view animated:YES completion:nil];
}

#pragma - LOCATION

-(void)initMapView{
    
    [mapView setDelegate:self];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    [mapView setMapType:MKMapTypeStandard];
    [self CenterLocationOnMap];
}

/**
 Method to clean all itens on map
 */
-(void)CleanMap{
    
    //Dismiss callout selection
    //    for (NSObject<MKAnnotation> *annotation in [[mapView annotations] copy]) {
    //        [mapView deselectAnnotation:(id <MKAnnotation>)annotation animated:NO];
    //    }
    
    [mapView removeAnnotations:[mapView annotations]];
    [mapView removeOverlays:[mapView overlays]];
    
}

-(void)LoadMap{
    
    //Clean first
    [self CleanMap];
    
    @try {
        
        //Add pin to customer location
        MKPointAnnotation *note = [[MKPointAnnotation alloc] init];
        note.title = currentCustomer.name;
        note.subtitle = [NSString stringWithFormat:@"Order: %i",currentCustomer.order];
        note.coordinate = currentCustomer.location.coordinate;
        [mapView addAnnotation:note];
        
        MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
        [request setSource:[MKMapItem mapItemForCurrentLocation]];
        [request setDestination:[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:currentCustomer.location.coordinate]]];
        [request setTransportType:MKDirectionsTransportTypeAny]; // This can be limited to automobile and walking directions.
        [request setRequestsAlternateRoutes:TRUE]; // Gives you several route options.
        MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
        [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
            if (!error) {
                for (MKRoute *route in [response routes]) {
                    [mapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads]; // Draws the route above roads, but below labels.
                    
                    //Show in KM if over 1 km
                    if (route.distance > 1000) {
                        [lblDistance setText:[NSString stringWithFormat:@"Distance: %.2f km",route.distance / 1000]];
                    }else{
                        [lblDistance setText:[NSString stringWithFormat:@"Distance: %.2f km",route.distance]];
                    }
                    
                    travelTime = [CKCoreTime GetHoursByTimeStamp:(double)route.expectedTravelTime];
                    
                    [lblTravelTime setText:[NSString stringWithFormat:@"Travel Time: %@",travelTime]];
                
                    [self zoomToPolyLine:mapView polyline:[route polyline] animated:YES];
                    
                }
                
            }else{
                NSLog(@"Error:%@",error.localizedDescription);
            }
        }];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

-(void)zoomToPolyLine:(MKMapView*)map polyline:(MKPolyline*)polyline animated: (BOOL)animated
{
    [map setVisibleMapRect:[polyline boundingMapRect] edgePadding:UIEdgeInsetsMake(40.0, 40.0, 40.0, 40.0) animated:animated];
}

/**
 Method to handle the pin property. This method assing a custom pin to site and user.
 
 @param mV         default
 @param annotation default
 
 @return default
 */
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    
    @try {
        
        //If annotation doesnt exist return nil
        if (!annotation) {
            return nil;
        }
        
        MKAnnotationView *annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"currentloc"];
        
        if (!annView){
            annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
        }
        
        if([annotation isKindOfClass:[MKUserLocation class]]){
            //User pin
            annView.image = [UIImage imageNamed:@"pin_black.png"];
        }else{
            //Site pin
            annView.image = [UIImage imageNamed:@"site.png"];
        }
        
        //NSLog(@"height: %f width: %f",annView.image.size.height,annView.image.size.width);
        return annView;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        [renderer setStrokeColor:[UIColor blueColor]];
        [renderer setAlpha:0.5];
        [renderer setLineWidth:4.0];
        
        return renderer;
    }
    
    return nil;
}

-(void)CenterLocationOnMap{
    
    @try {
        
        //Zoom user to center of the map
        //Set my location to center the map
        MKCoordinateRegion local = MKCoordinateRegionMakeWithDistance(CKPerson.location.coordinate, 8.5*1609.344, 8.5*1609.344);
        local.center.latitude = CKPerson.location.coordinate.latitude - 0.009;
        local.center.longitude = CKPerson.location.coordinate.longitude;
        local.span.latitudeDelta = 0.04f;
        local.span.longitudeDelta = 0.04f;
        [mapView setRegion:[mapView regionThatFits:local] animated:YES];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
