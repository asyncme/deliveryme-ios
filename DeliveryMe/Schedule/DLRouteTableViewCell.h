//
//  CKActionTableViewCell.h
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLRouteTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblPlaceName;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;
@property (nonatomic, weak) IBOutlet UILabel *lblCustomer;
@property (nonatomic, weak) IBOutlet UILabel *lblNote;

@property (nonatomic, weak) IBOutlet UIButton *btnAction;

@end
