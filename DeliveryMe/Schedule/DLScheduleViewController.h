//
//  UserConnectionViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLRouteTableViewCell.h"
#import "DLRouteViewController.h"
#import "DLDemoData.h"

@interface DLScheduleViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,CKCoreDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UIView *viewNoPlace;

-(IBAction)ShowMenu:(id)sender;
-(IBAction)Refresh:(id)sender;
-(IBAction)GoToPortal:(id)sender;

@end
