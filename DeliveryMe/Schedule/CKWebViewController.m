//
//  SBWebViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/06/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKWebViewController.h"

@interface CKWebViewController ()

@end

@implementation CKWebViewController

@synthesize webView,url;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [webView loadRequest:[[NSURLRequest alloc] initWithURL:url]];
    [webView setScalesPageToFit:TRUE];
    
}

-(IBAction)Close:(id)sender{
    [self.navigationController dismissViewControllerAnimated:TRUE completion:nil];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [[CKViewController sharedInstance] showClockInView:self.view];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[CKViewController sharedInstance] removeClock];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
