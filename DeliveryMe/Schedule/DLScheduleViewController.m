//
//  UserConnectionViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "DLScheduleViewController.h"

@interface DLScheduleViewController (){
    CKCoreWebService *CK;
    CKCoreLocation *CKLocation;
    NSMutableArray *array,*arrayNew,*arrayOld;
    NSTimer *refreshRow;
}

@end

@implementation DLScheduleViewController

@synthesize tblContent,viewNoPlace;

- (void)viewDidLoad{
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
}

-(IBAction)ShowMenu:(id)sender{

    DLMenuViewController *CKMenu = (DLMenuViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"CKMenu"];
    [CKMenu setImgReceive:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleLight]];
    [CKMenu setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:CKMenu animated:YES completion:nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self FetchData];
}

-(void)Refresh:(id)sender{
    [self FetchData];
}

-(IBAction)GoToPortal:(id)sender{

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:MAIN_HOST]];

}

/**
 Method to fecth data
 */
-(void)FetchData{
    
    array = [[NSMutableArray alloc] init];
    arrayNew = [[NSMutableArray alloc] init];
    arrayOld = [[NSMutableArray alloc] init];
    
    // If Demo user, load demo data
    if (CKPerson.personId == 0) {
        [arrayNew addObject:[DLDemoData getDemoRouteData]];
        [array addObject:arrayNew];
        
        if ([array count] == 0) {
            [tblContent setHidden:TRUE];
            [viewNoPlace setHidden:FALSE];
        }else{
            [tblContent setHidden:FALSE];
            [viewNoPlace setHidden:TRUE];
        }
        
        [tblContent reloadData];
        
    }else{
    
        [array removeAllObjects];
        [[CKViewController sharedInstance] showClockInView:self.view];
        [self performSelectorInBackground:@selector(RequestRoutes) withObject:nil];
        
    }
    
}

/**
 *  Method that will fetch all connections for the user
 */
-(void)RequestRoutes{
    
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&companyId=%i",CKPerson.personId,CKPerson.token,CKCompany.companyId];
    [CK CoreWebService:kRouteRoutes withRequest:myRequestString];
}


#pragma -mark SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_RoutesFetchSuccess) {
            
            for (NSMutableDictionary *dic in [dictionary valueForKey:@"routes"]) {
                
                DLRouteModel *route = [[DLRouteModel alloc] initObjectWithDictionary:dic];
                
                if ([CKCoreTime isTimeBeforeNow:route.time]) {
                    [arrayNew addObject:route];
                }else{
                    [arrayOld addObject:route];
                }
                
            }
            
            if ([arrayNew count] > 0) {
                [array addObject:[[self SortPlaceByTime:arrayNew] copy]];
            }
            
            if ([arrayOld count] > 0) {
                [array addObject:[[self SortPlaceByTime:arrayOld] copy]];
            }

            if ([array count] == 0) {
                [tblContent setHidden:TRUE];
                [viewNoPlace setHidden:FALSE];
            }else{
                [tblContent setHidden:FALSE];
                [viewNoPlace setHidden:TRUE];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [tblContent reloadData];
                });
            }

        }
        
        
    }else{
        
        if ([array count] == 0) {
            [tblContent setHidden:TRUE];
            [viewNoPlace setHidden:FALSE];
        }else{
            [tblContent setHidden:FALSE];
            [viewNoPlace setHidden:TRUE];
        }
        
    }
    
    [[CKViewController sharedInstance] removeClock];
    
}

-(NSArray *)SortPlaceByTime:(NSArray *)toSort{
    
    @try {
        
        NSSortDescriptor *sortDistance = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES comparator:^NSComparisonResult(id obj1, id obj2){
            DLRouteModel *route1 = (DLRouteModel *)obj1;
            DLRouteModel *route2 = (DLRouteModel *)obj2;
            
            return [[NSNumber numberWithDouble:route1.time] compare:[NSNumber numberWithDouble:route2.time]];
            
        }];
        
        return [toSort sortedArrayUsingDescriptors:@[sortDistance]];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        return nil;
    }
}


/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    [[CKViewController sharedInstance] removeClock];
    dispatch_async(dispatch_get_main_queue(), ^{
        [tblContent reloadData];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
}

#pragma -mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [array count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return [[array objectAtIndex:0] count];
    }else{
        return [[array objectAtIndex:1] count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *myIdentifier = @"cell";
    
    DLRouteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    if (cell == nil) {
        cell = [[DLRouteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
    //Validate if cell has been proper initialized before continue
    if (!cell) {
        return nil;
    }
    
    @try {
        
        DLRouteModel *routeModel;
        
        if (indexPath.section == 0) {
            routeModel = [[array objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }else{
            routeModel = [[array objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        

        [cell.lblPlaceName setText:[NSString stringWithFormat:@"Route: %@",routeModel.name]];
        [cell.lblTime setText:[NSString stringWithFormat:@"Date: %@",[CKCoreTime ConvertToLocalTime:routeModel.time]]];
        [cell.lblCustomer setText:[NSString stringWithFormat:@"Customers: %i",routeModel.qtdCustomers]];
        
        [cell.btnAction setTag:indexPath.row];
        [cell.btnAction addTarget:self action:@selector(viewRouteDetail:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tblContent deselectRowAtIndexPath:indexPath animated:NO];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UITableViewHeaderFooterView *headerView = [[UITableViewHeaderFooterView alloc] init];
    if ([array count] > 1) {
        if (section == 0) {
            [[headerView textLabel] setText:@"New Routes"];
        }else{
            [[headerView textLabel] setText:@"Previous Routes"];
        }
    }else{
        if ([arrayNew count] > 0) {
            [[headerView textLabel] setText:@"New Routes"];
        }else{
            [[headerView textLabel] setText:@"Previous Routes"];
        }
    }
    
    return headerView;
}

-(void)viewRouteDetail:(UIButton *)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblContent];
    NSIndexPath *indexPath = [self.tblContent indexPathForRowAtPoint:buttonPosition];
    
    DLRouteModel *routeModel = [[array objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    //Go to detail view of the route
    DLRouteViewController *view = (DLRouteViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"RouteView"];
    [view setRouteModel:routeModel];
    [view setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:view] animated:YES completion:nil];
    
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    });
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[CKViewController sharedInstance] removeClock];
}

@end
